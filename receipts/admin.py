from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt

# Register your models here
# this is where you need to remember the @admin function
# if they don't work, make sure each name is in quotes in models
# keep these classes seperate from CLASSES YOU WILL CREATE FOR EXPENSES
admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)
