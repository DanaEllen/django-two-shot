from django.urls import path
from .views import (
    account_list,
    category_list,
    create_category,
    receipt_list,
    create_account,
)
from .views import create_receipt

# if you get confused on the path just remember it starts
# in the parent directory


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
