from django.db import models
from django.contrib.auth.models import User

# Create your models here


# THIS CLASS WILL CREATE A MODEL FOR CATAGORIZING EXPENSES
class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User, related_name="categories", on_delete=models.CASCADE
    )


# THIS CLASS CREATES WILL SHOW HOW WE PAID FOR THINGS.
# REMEMBER TO USE A CAPITAL A WHEN REFERING TO THIS CLASS
class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User, related_name="accounts", on_delete=models.CASCADE
    )


# THIS IS A CLASS FOR THE RECEIPT MODEL. THERE ARE A LOT OF SPECIFICATIONS HERE
# USE A CHECKLIST TO MAKE SURE YOU GOT THEM ALL.


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        User, related_name="receipts", on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        "ExpenseCategory", related_name="receipts", on_delete=models.CASCADE
    )
    account = models.ForeignKey(
        "Account", related_name="receipts", on_delete=models.CASCADE, null=True
    )
